---
title: "ToW #7 - 在 C++ & C# 中設定並取得檔案屬性中的版本號碼"
date: 2022-08-27T00:00:00Z
tags: ["Tips of Week", "C++", "C-Sharp"]
summary: ""
---

## C++ 專案

### 設定版本號碼

1. 對專案按下右鍵，並選擇 「加入」 → 「資源」。
2. 在跳出的視窗中選擇 「Version」，接著點選 「新增」。
3. 編輯 _xxx.rc_ 中的 `FileVersion` 及 `ProductVersion` 欄位。
4. 重新編譯專案後，即可在檔案屬性中看到版本號碼。

參考資料：[Compiling DLL with Version Information](https://stackoverflow.com/a/2618695)

## C# 專案

### 設定版本號碼

1. 編輯專案中的 _AssemblyInfo.cs_。
2. 在裡面新增以下程式碼：

```csharp
// 設定 ProductVersion
[assembly: AssemblyInformationalVersion("1.0.0.0")]
// 設定 FileVersion
[assembly: AssemblyFileVersion("1.0.0.0")]
```

3. 重新編譯專案後，即可在檔案屬性中看到版本號碼。

參考資料：[Set Product Version and File Version to different numbers in C#](https://stackoverflow.com/a/60113769)

### 取得版本號碼

```csharp
Assembly assembly = Assembly.GetExecutingAssembly();
FileVersionInfo fileVersionInfo = FileVersionInfo.GetVersionInfo(assembly.Location);
string productVersion = fileVersionInfo.ProductVersion;
string fileVersion = fileVersionInfo.FileVersion;
```

參考資料：[How to get the current product version in C#?](https://stackoverflow.com/a/7382420)
