---
title: "ToW #1 - 第一個禮拜就不少問題"
date: 2022-05-30T09:51:34Z
tags: ["Tips of Week", "VSCode", "Hugo", "WSL", "Certbot"]
summary: "VS Code, Hugo and so on"
---

## VS Code

### Port Forward 無法使用，導致無法使用 SSH 連線至遠端進行編輯

起因是因為我用 SSH 連線至樹莓派的時候，一直連不進去，發現Remote-SSH 輸出了這個錯誤訊息：

```
...
Failed to set up socket for dynamic port forward to remote port xxxxxx
...
```
於是 Google 了一下，發現了[這個 Issue](https://github.com/microsoft/vscode-remote-release/issues/4683)，並且有人提供[解決方法](https://github.com/microsoft/vscode-remote-release/issues/4683#issuecomment-1059753371)。

大致上就是要啟用 SSH daemon 的 `AllowTcpForwarding`，於是：

```shell
$ sudo vim /etc/ssh/sshd_config # 找到 AllowTcpForwarding，並修改為 yes
$ sudo systemctl restart sshd # 重啟 sshd
```

接著就能成功連線了！

## Hugo

### 設定 PaperMod 主題啟用 Google Analytics

首先透過 Google Analytics 取得 _評估 ID_（e.g. G-XXXXXXXXXX）。

接著在 Hugo 設定檔中加入以下參數：

```yaml
params:
  analytics:
    google:
      SiteVerificationTag: G-XXXXXXXXXX
```

接著使用 `production` 環境產生網頁即可。

```bash
$ hugo -e production
```

如果要避免自己在測試時影響到分析資料，使用非 `production` 環境產生網頁就能避免在網頁中加入 Google Analytics。

### 設定 PaperMod 主題啟用編輯文章連結

在 Hugo 設定檔中加入一下參數：

```yaml
editPost:
	URL: "https://gitlab.com/weikeup/weikeup.io/-/tree/main/content"
	appendFilePath: true
```

## WSL

### DNS 無法解析

有時候在 WSL 會突然遇到沒辦法用網址連線，用 IP 卻正常的情況，測試後發現是 DN 無法解析的問題。

因此可以透過手動指定 DNS 位址來解決問題。

1. 先建立 `wsl.conf` 並設定不要自動建立 `resolv.conf`。

```bash
$ sudo cat << EOF > /etc/wsl.conf
[network]
generateResolvConf = false
EOF
```

2. 刪除原本的 `resolv.conf`，並重新指定 DNS 位址。

```bash
$ sudo rm -f /etc/resolv.conf
$ sudo cat << EOF > /etc/resolv.conf
nameserver xxx.xxx.xxx.xxx
EOF
```

> Note. 如果 WSL 重啟後，`resolv.conf` 的內容消失的話，需設定成唯讀檔避免被修改。
>
> ```bash
> $ sudo chattr +i /etc/resolv.conf
> ```

3. 接著就能重啟 WSL 了。

```bash
$ wsl.exe --terminate Ubuntu-20.04 # 請自行更換名稱，可以使用 wsl -l 來查看名稱
```

## Certbot

### 在 Renew 憑證之前及之後，執行對應的 Script。

如果使用 Standalong 模式進行憑證驗證的話，在 Renew 時會需要使用到 Port 80，因此需要先將已使用該通訊埠的程式（通常是 Web server，如 Nginx）先停止，等到 Renew 完畢後，再將程式啟動。

可以透過在 `/etc/letsencrypt/renewal-hooks/` 中，在 `pre`、`deploy` 及 `post` 資料夾下新增要執行的 Shell Script 即可在 Renew 時，依照不同的階段執行腳本。

_此處以啟動/停止 Nginx 作為範例。_

1. 在 `pre` 中新增停止 Nginx 的腳本。
```bash
$ cat << EOF > stop_nginx.sh
#!/bin/sh
docker stop nginx # 這裡使用 Docker 上的 Nginx 容器
EOF
$ chmod +x stop_nginx.sh
```

2. 在 `post` 中新增啟動 Nginx 的腳本。
```bash
$ cat << EOF > start_nginx.sh
#!/bin/sh
docker start nginx # 這裡使用 Docker 上的 Nginx 容器
EOF
$ chmod +x start_nginx.sh
```

3. 使用 `--dry-run` 測試腳本是否如期執行。
```bash
$ sudo certbot renew --dry-run
```

輸出結果：
```
...
Running pre-hook command: /etc/letsencrypt/renewal-hooks/pre/stop_nginx.sh
... # 開始更新憑證
Running post-hook command: /etc/letsencrypt/renewal-hooks/post/start_nginx.sh
...
```
