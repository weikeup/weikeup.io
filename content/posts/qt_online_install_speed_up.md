---
title: "Qt Online Installer 4.x 下載過慢"
date: 2022-05-28T15:54:21Z
tags: ["Qt"]
summary: "透過切換下載伺服器來提升下載速度"
---

# Qt Online Installer 4.x 下載過慢

最近在安裝 Qt 的時候發現下載的速度很慢，上網查了一下看到有很多人有同樣地問題，於是隨手記錄一下解決方法。

一開始在下載元件的時候，筆者透過 Wireshark 查了一下下載伺服器的位置，發現是從上海的一個伺服器下載檔案。

由於 Qt 官方有開放大家可以做下載鏡像伺服器，筆者發現亞洲的部分在中國跟日本各有兩個伺服器，而 Qt Online Installer 在判斷要使用哪個伺服器來下載檔案是透過 IP 的地理位置來決定的，所以從臺灣 IP 下載的話，會連到上海的伺服器，造成下載速度低落且不穩定。

解決方法很簡單，就是更換伺服器位置！

經過測試，連到日本的伺服器比上海的伺服器下載還快好幾倍，整體來講大概就是下載時間從幾小時縮短到幾分鐘，差異非常大！

## 操作步驟

1. 首先至 Qt 官網下載線上安裝包。
![Qt 官網](/images/qt_online_install_speed_up.qt_homepage.png)
2. 透過命令提示字元 (cmd.exe) 執行安裝包，並使用 `--mirror <url>` 指定鏡像伺服器位置。

_這裡使用日本 JAIST 之鏡像伺服器作為下載來源。_

```bash
qt-unified-windows-x86-4.1.0-online.exe --mirror http://ftp.jaist.ac.jp/pub/qtproject/
```
3. 接下來的步驟就跟正常安裝方法一樣。

經過筆者實測，下載速度可以從幾 kBps 提升到 5 MBps 左右！

---

鏡像伺服器列表：[Qt Downloads](https://download.qt.io/static/mirrorlist/)（進入後點擊 "HTTP" 的網址即為鏡像位址）
