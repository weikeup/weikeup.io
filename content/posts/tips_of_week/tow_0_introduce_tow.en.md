---
title: "ToW #0 - Introduce Tips of Week"
date: 2022-05-28T09:29:20Z
tags: [ "Tips of Week" ]
summary: "What is Tips of Week？"
---

# What is Tips of week?

Sometime I met some problems, such as forget the usage of commands, settings problem, and learn some new things, etc.

I want to record these problems after I resolve them. Because if I get into those problems again in the future, I can find the solutions immediately in my old posts.

Therefore, I decide to start writing the series of Tips of Week (ToW) to record every problems what I met in every weeks.

In this series, I will write less content to describe the problems. If the description is too long, I'll post it at another page, and use a URL instead.

Finally, this is the end of introduction. Please look forward to my posts in every weeks! See you~
