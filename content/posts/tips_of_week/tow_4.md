---
title: "ToW #4 - Linux Time"
date: 2022-06-27T00:00:00Z
tags: ["Tips of Week", "Linux", "iBus", "Rime", "Docker", "Snap", "OpenWRT"]
summary: "在安裝完全新的 Linux 後，遇到了一些問題..."
---

## Linux

### iBus-daemon 無法自動啟動

使用 `im-config` 進行輸入法設定，並**重新開機（必要！）**。

### 調整 Rime 輸入法文字大小

進到 IBus Preferences，並修改 General → Use custom font 即可。

### 快速安裝 Docker

咻

```shell
$ curl https://get.docker.com/ | sh
```

### 無法從 Snap 安裝 telegram-desktop

通常是 Snapd 版本的問題，升級後就能安裝了。

```shell
$ sudo snap install core snapd
$ sudo snap install telegram-desktop
```

### Renew OpenWRT DHCP IP

On OpenWRT:

```shell
$ vim dhcp.leases # Remove the target client
$ /etc/init.d/dnsmasq restart
```

On Client:

```shell
$ sudo dhclient
```
