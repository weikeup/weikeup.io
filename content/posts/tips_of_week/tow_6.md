---
title: "ToW #6 - Linux and Spring"
date: 2022-08-18T00:00:00Z
tags: ["Tips of Week", "Linux", "Spring"]
summary: "Linux and Spring"
---

## Linux

### 升級 Debian 10(Buster) 至 11(Bullseye)

1. 將 `/etc/apt/source.list` 裡面的 **buster** 全部改成 **bullseye**
2. 將 `/etc/apt/source.list.d/` 底下的檔案內的 **buster** 全部改成 **bullseye**
3. 執行 `sudo apt update && sudo apt upgrade` 開始進行更新。
4. 更新完成後，建議進行重開機 `sudo reboot`。

### 升級 Debian 版本後，使用 apt 安裝套件時發生 _libc6-dev : Breaks: libgcc-8-dev (< 8.4.0-2~) but 8.3.0-6 is to be installed_

安裝 `gcc-8-base` 後即可解決此問題。

```shell
$ sudo apt install gcc-8-base
```

### 設定網路校準時間

安裝 `ntp` 套件後，啟用 `ntpd` 服務來定時校準系統時間。

```shell
$ sudo apt install ntp # 安裝 ntp。
$ sudo systemctl start ntpd # 啟動 ntpd 服務。
```

## Spring

### 取得 Spring Proxy 中實際的類別

當使用 Spring AOP 時，從 Spring 取得的 Bean 會是一個 Proxy 類別。可以透過 `AopTestUtils.getTargetObject(proxy)` 來取得實際被代理的物件。

#### 範例程式
```java
// getBean 實際上會回傳一個被代理的 MyClass 物件。
MyClass myClass = applicationContext.getBean("myClass", MyClass.class);
// 取得實際的物件。
MyClassImpl myClassImpl = AopTestUtils.getTargetObject(myClass);
```

參考資料：[Casting a Spring's Proxy object to the actual runtime class](https://stackoverflow.com/a/58175289)

### 為什麼 `method2` 不會執行 AOP method？

因為 `method2` 是在類別內部被呼叫的，而不是透過 AspectJ Proxy 呼叫，因此 AOP 不會作動。

#### 範例程式

```java
interface Example {
    
    void method1();
    void method2();
}

class ExampleImpl implement Example {
    
    void method1() {
        method2(); // This will NOT be logged!
        // Do something...
    }

    void method2() {
        // Do something...
    }
}

// Spring AOP
@Component
@Aspect
class AspectLogger {
    
    @After("execution(* Example.method1(..))")
    void logMethod1() { /* Logging method1 */ }

    // This will NOT be executed when method1 be called!
    @After("execution(* Example.method2(..))")
    void logMethod2() { /* Logging method2 */ }
}

// Call method1 through Bean
Example example = ctx.getBean(ExampleImpl.class);
example.method1();
```