---
title: "About Me"
date: 2022-05-04T08:14:55Z
tags: ["About Me"]
---

# 嗨! 我是 Weikeup 👋

我的名字叫做 Weikeup，發音為 "wake up"。

在這個部落格裡，我會分享許多跟程式有關的內容。

如果你也對程式有興趣的話，不妨來看看我寫的文章吧！
