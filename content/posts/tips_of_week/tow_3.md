---
title: "ToW #3 - 懶惰週"
date: 2022-06-19T16:07:10Z
tags: ["Tips of Week", "Netlify", "Python"]
summary: "Netlify, and Python"
---

## Netlify

### Deployed filenames cannot contain # or ? characters

由於我在文章內的 `tags` 中使用 "C#" 來當作標籤，因此 Hugo 在產生標籤頁面的時候，會造成類似 `tags/c#/` 這樣的檔案路徑，導致部屬到 Netlify 的時候引發路徑名稱錯誤。

可以參考[這邊](https://stackoverflow.com/a/66282068/7275697)，透過將標籤命名為 "C-Sharp" 之後，在網頁中有需要出現 "C-Sharp" 的地方，透過取代功能，將 "-Sharp" 取代成 "#"，如此一來，Hugo 產生的檔案路徑就會像是 `tags/C-Sharp` 這樣，然後在網頁中則是顯示 "C#"。

網頁中修改後的元素如下：

```html
<a href="{{ "/tags/" | relLangURL }}{{ . | urlize }}/">{{ replace . "-Sharp" "#" }}</a>
```

## Python

### 啟動簡易網頁伺服器

On Python2:

```shell
python -m SimpleHTTPServer [port] [--bind <address>]
```

On Python3:

```shell
python3 -m http.server [port] [--bind <address>]
```