---
title: "ToW #2 - 第二個禮拜"
date: 2022-06-09T15:51:34Z
tags: ["Tips of Week", "C-Sharp", "OpenCV"]
summary: "C# and OpenCV"
---

## C#

### 執行程式時，發生 `BadImageFormatException`

起因是我在專案中使用 OpenCvShape，但是執行程式時會產生 `BadImageFormatException`。

因此 Google 了一下後，發現是因為專案設定了 `建議使用 32 位元` 的關係。

這項設定導致編譯出來的程式在執行時期，會因為載入無效的檔案映像檔，而引發 `BadImageFormatException`。

因此只要拿掉 `建議使用 32 位元` 這個選項，就沒問題了。

## OpenCV

### 影片的最後一框無法被讀取

根據 [這個解答](https://stackoverflow.com/a/26760959/7275697) 的做法，在讀取框之前及之後記錄下 `CV_CAP_PROP_POS_FRAMES` 的值，並當後取得的值小於前取得的值時，表示已經讀取到影片末端。

```cpp
int beforeReadFramePosition = videoCapture.get(CV_CAP_PROP_POS_FRAMES);
bool result = videoCapture.read(mat);
int afterReadFramePosition = videoCapture.get(CV_CAP_PROP_POS_FRAMES);

if (afterReadFramePosition <= beforeReadFramePosition) {
  // End of Video
}
```