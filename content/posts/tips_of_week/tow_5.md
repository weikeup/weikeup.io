---
title: "ToW #5 - NextCloud"
date: 2022-07-04T00:00:00Z
tags: ["Tips of Week", "NextCloud"]
summary: "NextCloud Tips"
---

## NextCloud

### 設定上傳區塊

NextCloud 預設上傳區塊為 10MB，若上傳網速大於 10MB 的話，可以把上傳區塊調大，提高上傳效率。

_CHUNK_SIZE 為上傳區塊大小 (byte)，設定為 0 表示無限制。_

```sh
sudo -u www-data php occ config:app:set files max_chunk_size --value CHUNK_SIZE
```

參考：[Adjust chunk size on Nextcloud side](https://docs.nextcloud.com/server/latest/admin_manual/configuration_files/big_file_upload_configuration.html#adjust-chunk-size-on-nextcloud-side)

### 手動掃描檔案

- 掃描特定目錄

  ```sh
  # Path format: USER_NAME/files/TARGET_DIR
  # e.g. tom/files/Videos
  sudo -u www-data php occ files:scan --path PATH
  ```

- 掃描特定使用者目錄

  ```sh
  # Example for user name: tom
  sudo -u www-data php occ files:scan USER_NAME
  ```