---
title: "在 Raspberry Pi 設定 HP 印表機並透過網路列印文件"
date: 2022-07-21T16:52:45Z
tags: ["Raspberry Pi", "HP Printer"]
summary: "安裝及設定 cups。"
---

## 本文測試環境

- Rasbian 10 armv7l
- HP Color LaserJet CP1215

## 前置準備

### 安裝 `cups`、`hplip` 及 `colord`

```shell
$ sudo apt install cups hplip colord
```

### 設定 cups 允許從外網連線

1. 允許外網存取 cups。

    a. 啟用外網存取。

    ```shell
    $ sudo cupsctl --remote-any
    ```

    b. 開啟 `/etc/cups/cupsd.conf`。

    ```shell
    $ sudo vim /etc/cups/cupsd.conf
    ```

    c. 在檔案最上方新增以下設定：

    ```
    ServerAlias *
    ```

    d. 重新啟動 cupsd。

    ```shell
    $ sudo systemctl restart cups
    ```

2. 安裝 HP 擴充功能。

```shell
$ sudo hp-plugin
```

3. 將 pi 使用者設定為印表機管理員。

```shell
$ sudo usermod -aG lpadmin pi
```

## 新增印表機

1. 將印表機透過 USB 連接到 RPI。
2. 使用網頁瀏覽器連線到 `https://<RPI_IP>:631/`
3. 切換到 _Administration_ 分頁，並點選 _Add Printer_。
4. 選擇欲新增的印表機，並點選 _Continue_。
5. 命名該印表機，並勾選 _Share This Printer_，再點選 _Continue_。
6. 選擇合適的印表機驅動程式，並點選 _Continue_。
7. 點選 _Set Default Option_，完成新增印表機。
8. 在印表機資訊頁面點選 _Maintenance_ → _Print Test Page_ 來列印測試頁面。

## 在 Windows 新增網路印表機

1. _開始_ → _設定_ → _裝置_ → _印表機&掃瞄器_ → _新增印表機或掃瞄器_。
2. 點選 _手動新增印表機_。
3. 選擇 _依名稱選取共用的印表機_，並填入以下網址：
  - 注意！此處必須使用 **http**。

```
http://<RPI_IP>:631/printers/<PRINTER_NAME>
範例：http://pi4.lan:631/printers/HP_CP1215
```

4. 選擇合適的印表機驅動程式。
5. 完成新增印表機，可點選 _列印測試頁面_ 來測試設定是否成功。

## 參考資料

- [How to Make a Raspberry Pi-Powered Print Server](https://www.tomshardware.com/how-to/raspberry-pi-print-server)
- [CUPS bad request](https://stackoverflow.com/a/54812455)
- [HP打印機在cups下打印報錯"Filter failed"](https://zhuanlan.zhihu.com/p/457045615)